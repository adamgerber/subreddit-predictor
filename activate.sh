#!/bin/bash

if [ "$0" = "-bash" ]; then
    source env/dev/bin/activate
else
    echo 'Error: must run using "source" (or "."), not "sh"'
    exit
fi
