# -*- coding: utf-8 -*-

from __future__ import division
from io import open
from collections import defaultdict, Counter, OrderedDict
import re
import json
import click

import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


def process_text(text):
    text = re.sub('\n', ' ', text)
    text = re.sub(r'[*#=_,?!.;:"/()\[\]-]', ' ', text)
    text = re.sub(r'[ \t]+', ' ', text)
    return text.lower().split(' ')
    

def count_words(raw_file, total_lines):
    with open(raw_file) as f:
        for line in f:
            row = json.loads(line)
            tokens = process_text(row[1])

            log.debug(tokens)


def count_words_by_subreddit(raw_file, total_lines):
    counts = defaultdict(Counter)
    verbs_by_subreddit = defaultdict(Counter)
    with open(raw_file) as f:
        for line in f:
            row = json.loads(line)
            subreddit = row[0]
            tokens = process_text(row[1])
            counts[subreddit].update(tokens)

            for token in tokens:
                if token.endswith('ing') or token.endswith('ed'):
                    verbs_by_subreddit[subreddit][token] += 1

            #log.debug(tokens)

    for subreddit in verbs_by_subreddit:
        log.info('{}: {}'.format(subreddit, verbs_by_subreddit[subreddit]))


def get_top100_subreddits(top100_path):
    subreddits = set()
    with open(top100_path) as f:
        for line in f:
            if line.startswith('#'):
                continue

            subreddits.add(line.strip())
    return subreddits

    
def count_words_by_subreddit_top100(raw_file, total_lines, top100_path):
    top100_subreddits = get_top100_subreddits(top100_path)
    counts = defaultdict(Counter)
    verbs_by_subreddit = defaultdict(Counter)
    verbs_global = Counter()
    subject_pronouns = {'i', 'you', 'he', 'she', 'we', 'they'} 
    # linking_verbs = {'is', 'are', 'was'}
    with open(raw_file) as f:
        for line in f:
            row = json.loads(line)
            subreddit = row[0]
            if subreddit not in top100_subreddits:
                continue
            tokens = process_text(row[1])
            counts[subreddit].update(tokens)

            for i, token in enumerate(tokens):
                to_add = None
                if token.endswith('ing') and len(token) > 6:
                    token_to_add = token
                elif token.endswith('ed') and len(token) > 6:
                    token_to_add = token
                elif token in subject_pronouns:
                    try:
                        token_to_add = tokens[i + 1]
                    except:
                        pass

                if to_add:
                    verbs_by_subreddit[subreddit][token_to_add] += 1
                    verbs_global[token_to_add] += 1

            #log.debug(tokens)

    filename = 'out.txt'
    with open(filename, 'w') as f:

        for subreddit in sorted(verbs_by_subreddit.keys()):
            f.write('= {} ===\n'.format(subreddit))

            for word, count in verbs_by_subreddit[subreddit].most_common():
                f.write('{:5}: {}\n'.format(count, word))


@click.group()
def cli():
    pass

@cli.command('count_words')
@click.argument('raw_file', type=str)
@click.option('--num_lines', default=None)
def service_count_words(raw_file, num_lines):
    if num_lines:
        log.debug('num lines: {}'.format(num_lines))
    count_words(raw_file, num_lines)

@cli.command('count_words_by_subreddit')
@click.argument('raw_file', type=str)
@click.option('--num_lines', type=int, default=None)
@click.option('--top100', type=str, default=None)
def service_count_words_by_subreddit(raw_file, num_lines, top100):
    if num_lines:
        log.debug('num lines: {}'.format(num_lines))

    if top100:
        count_words_by_subreddit_top100(raw_file, num_lines, top100)
    else:
        count_words_by_subreddit(raw_file, num_lines)


if __name__ == '__main__':
    cli()

