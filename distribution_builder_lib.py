# -*- coding: utf-8 -*-

from __future__ import division

from image_extractor_lib import BagOfWordsImageCorpusBuilder


class DistributionBuilderBase(object):
    @staticmethod
    def build_vocab(model):
        return sorted(model.keys())


class RawDistributionBuilder(DistributionBuilderBase):
    """Builds a distribution of a specified language model over the vocabulary of
    a reference language model, using raw occurrence counts"""
    SHORT_NAME = 'raw'
    OOV_SCORE = 0.001

    def __init__(self, models, reference_subreddit):
        self.vocab = self.build_vocab(models[reference_subreddit])

    def get_distribution(self, model):
        # performance tweak to avoid the repeated lookups to self, as this will be called repeatedly
        oov_score = self.OOV_SCORE
        return [model.get(word, oov_score) for word in self.vocab]


class NormalizedDistributionBuilder(DistributionBuilderBase):
    """Builds a distribution of a specified language model over the vocabulary of
    a reference language model, using occurrence counts normalized over the entire
    vocabulary of the specified language model"""
    SHORT_NAME = 'norm'
    OOV_SCORE = 0.001

    def __init__(self, models, reference_subreddit):
        self.vocab = self.build_vocab(models[reference_subreddit])

    @staticmethod
    def compute_normalizing_factor(values):
        return 1.0 / sum(values)

    def get_distribution(self, model):
        # performance tweak to avoid the repeated lookups to self, as this will be called repeatedly
        oov_score = self.OOV_SCORE
        # compute normalizing constant
        z = self.compute_normalizing_factor(model.itervalues())

        return [model.get(word, oov_score) * z for word in self.vocab]


class SubsetNormalizedDistributionBuilder(DistributionBuilderBase):
    """Builds a distribution of a specified language model over the vocabulary of
    a reference language model, using occurrence counts normalized over the
    intersection of the vocabularies of language model"""
    SHORT_NAME = 'norm_intersect'
    OOV_SCORE = 0.001

    def __init__(self, models, reference_subreddit):
        self.vocab = self.build_vocab(models[reference_subreddit])

    def get_distribution(self, model):
        # performance tweak to avoid the repeated lookups to self, as this will be called repeatedly
        oov_score = self.OOV_SCORE
        # compute normalizing constant
        vocab = self.vocab
        z = 1.0 / (1 + sum(model[x] for x in model if x in vocab))
        return [model.get(word, oov_score) * z for word in vocab]


class BackgroundReducingDistributionBuilder(DistributionBuilderBase):
    """Builds a distribution of a specified language model over the vocabulary of
    a reference language model, using raw occurrence counts that are reduced in
    proportion to their raw occurrence over all subreddits"""
    SHORT_NAME = 'raw_bg'
    OOV_SCORE = 0.001

    def __init__(self, models, reference_subreddit):
        self.vocab = self.build_vocab(models[reference_subreddit])

        self.model_all = models[BagOfWordsImageCorpusBuilder.ALL]

    def get_distribution(self, model):
        oov_score = self.OOV_SCORE
        model_all = self.model_all
        # performance tweak to avoid the repeated lookups to self, as this will be called repeatedly
        return [model.get(word, oov_score) / model_all.get(word) for word in self.vocab]


class NormalizedBackgroundReducingDistributionBuilder(DistributionBuilderBase):
    """Builds a distribution of a specified language model over the vocabulary of
    a reference language model, using normalized occurrence counts that are reduced in
    proportion to their normalized occurrence over all subreddits"""
    SHORT_NAME = 'norm_bg'
    OOV_SCORE = 0.001
    UNDERFLOW_SCORE = 0.00001

    def __init__(self, models, reference_subreddit):
        self.vocab = self.build_vocab(models[reference_subreddit])

        self.model_all = models[BagOfWordsImageCorpusBuilder.ALL]
        self.z_all = self.compute_normalizing_factor(self.model_all.itervalues())

    @staticmethod
    def compute_normalizing_factor(values):
        return 1.0 / sum(values)

    def get_distribution(self, model):
        # performance tweak to avoid the repeated lookups to self, as this will be called repeatedly
        z_all = self.z_all
        underflow_score = self.UNDERFLOW_SCORE
        oov_score = self.OOV_SCORE
        model_all = self.model_all

        # compute normalizing constant
        z = self.compute_normalizing_factor(model.itervalues())

        return [max(underflow_score,
                    z * model.get(word, oov_score) - z_all * model_all[word])
                for word in self.vocab]


class L2BackgroundReducingDistributionBuilder(DistributionBuilderBase):
    """Builds a distribution of a specified language model over the vocabulary of
    a reference language model, using squared, normalized occurrence counts that are
    reduced in proportion to their squared, normalized occurrence over all subreddits"""
    SHORT_NAME = 'l2_norm_bg'
    OOV_SCORE = 0.001
    UNDERFLOW_SCORE = 0.00001

    def __init__(self, models, reference_subreddit):
        self.vocab = self.build_vocab(models[reference_subreddit])

        self.model_all = models[BagOfWordsImageCorpusBuilder.ALL]
        self.z_all = self.compute_normalizing_factor(self.model_all.itervalues())

    @staticmethod
    def compute_normalizing_factor(values):
        return 1.0 / sum(x ** 2 for x in values)

    def get_distribution(self, model):
        # compute normalizing constant
        z = self.compute_normalizing_factor(model.itervalues())

        # performance tweak to avoid the repeated lookups to self, as this will be called repeatedly
        z_all = self.z_all
        underflow_score = self.UNDERFLOW_SCORE
        oov_score = self.OOV_SCORE
        model_all = self.model_all

        return [max(underflow_score,
                    z * max(model.get(word, 0) ** 2, oov_score) - z_all * model_all[word] ** 2)
                for word in self.vocab]
