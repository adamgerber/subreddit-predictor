# -*- coding: utf-8 -*-

from __future__ import division
import errno
from os import makedirs
from os.path import isdir, join
from sys import stdout
from io import open
from time import time
from collections import defaultdict, OrderedDict, namedtuple

import numpy as np
from scipy.stats import entropy, energy_distance
from sklearn.metrics import mutual_info_score

import click

from image_extractor_lib import BagOfWordsImageCorpusBuilder
from distribution_builder_lib import (RawDistributionBuilder,
                                      NormalizedDistributionBuilder,
                                      SubsetNormalizedDistributionBuilder,
                                      BackgroundReducingDistributionBuilder,
                                      NormalizedBackgroundReducingDistributionBuilder,
                                      L2BackgroundReducingDistributionBuilder)

import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


# define metrics
MetricType = namedtuple('MetricType', 'metric name sorter')
entropy_metric = MetricType(metric=entropy, name='entropy', sorter=lambda x: x[0])
energy_metric = MetricType(metric=energy_distance, name='energy', sorter=lambda x: x[0])
mutual_metric = MetricType(metric=mutual_info_score, name='mutual', sorter=lambda x: -x[0])

# metrics that can be specified from the command line
available_metrics = OrderedDict()
for m in [entropy_metric, energy_metric, mutual_metric]:
    available_metrics[m.name] = m

# metrics that run when no metric is specified from the command line
active_metrics = OrderedDict()
for m in [entropy_metric, energy_metric]:
    active_metrics[m.name] = m


# define counters
available_counters = OrderedDict()
for c in [RawDistributionBuilder,
          NormalizedDistributionBuilder,
          SubsetNormalizedDistributionBuilder,
          BackgroundReducingDistributionBuilder,
          NormalizedBackgroundReducingDistributionBuilder,
          L2BackgroundReducingDistributionBuilder]:
    available_counters[c.SHORT_NAME] = c

active_counters = OrderedDict()
for c in [RawDistributionBuilder,
          NormalizedDistributionBuilder,
          BackgroundReducingDistributionBuilder,
          NormalizedBackgroundReducingDistributionBuilder,
          L2BackgroundReducingDistributionBuilder]:
    active_counters[c.SHORT_NAME] = c


def timed_task(func):
    """Used as a decorator to log the time taken by a function"""
    def wrapper(*args, **kwargs):
        name = func.__name__
        start = time()

        log.info("beginning {}...".format(name))
        result = func(*args, **kwargs)
        log.info("completed {}: {:7.2f} ms".format(name, 1000 * (time() - start)))

        return result
    return wrapper


@timed_task
def load_language_models(path):
    """Load the language model specified by the user"""
    models = defaultdict(dict)
    with open(path, 'rb') as f:
        d = np.load(f)
        for subreddit in d.iterkeys():
            models[subreddit] = d[subreddit].tolist()

    return models


@timed_task
def rank_subreddits_by_similarity(metric, counter_class, target_subreddit, language_models):
    subreddits_to_skip = {target_subreddit, BagOfWordsImageCorpusBuilder.ALL, BagOfWordsImageCorpusBuilder.OTHER}

    get_distribution = counter_class(language_models, target_subreddit).get_distribution

    target_subreddit_model = language_models[target_subreddit]
    target_subreddit_counts = get_distribution(target_subreddit_model)

    scores = []

    log.info('scoring for "{}" with "{}/{}"...'.format(target_subreddit, metric.name, counter_class.SHORT_NAME))
    for subreddit, subreddit_model in language_models.iteritems():
        if subreddit in subreddits_to_skip:
            continue

        subreddit_counts = get_distribution(subreddit_model)
        subreddit_score = metric.metric(target_subreddit_counts, subreddit_counts)

        scores.append((subreddit_score, subreddit))

    log.info('scoring done.')
    return sorted(scores, key=metric.sorter)


def display_output(ranked_subreddits, output, label):
    output_text = "".join('{:.3f}   {}\n'.format(score, subreddit) for score, subreddit in ranked_subreddits)
    if output is stdout:
        stdout.write(label + '\n')
        stdout.write(output_text)
    else:
        try:
            makedirs(output)
        except OSError as exc:
            if exc.errno != errno.EEXIST or not isdir(output):
                raise

        output_path = join(output, label)

        with open(output_path, 'w') as f:
            f.write(unicode(output_text))


def process_subreddits(subreddits, language_models, metrics, counters, output):
    if output is not stdout:
        hex_time = hex(int(time()))[2:]
        output = join(output, hex_time)
        log.info('writing output to "{}".'.format(output))

    for subreddit in subreddits:
        for metric_name, metric in metrics.iteritems():
            for counter_name, counter in counters.iteritems():
                ranked_subreddits = rank_subreddits_by_similarity(metric, counter, subreddit, language_models)
                display_output(ranked_subreddits, output, '{}-{}-{}'.format(subreddit, metric_name, counter_name))


@click.group()
def cli():
    pass


@cli.command('rank')
@click.argument('language_model_file', type=click.Path(exists=True))
@click.option('--subreddit', type=click.STRING, default=None)
@click.option('--metric', type=click.Choice(available_metrics.keys()), default=None)
@click.option('--counter', type=click.Choice(available_counters.keys()), default=None)
@click.option('--output', type=click.STRING, default=None)
def rank(language_model_file, subreddit, metric, counter, output):
    language_models = load_language_models(language_model_file)
    metrics = {metric: active_metrics[metric]} if metric else active_metrics
    counters = {counter: active_counters[counter]} if counter else active_counters

    if output is None:
        output = stdout

    # if a subreddit is specified, set it up for processing,
    # otherwise process all subreddits in the language model file
    if subreddit is None:
        subreddits = language_models.keys()
        subreddits.remove(BagOfWordsImageCorpusBuilder.ALL)
        subreddits.remove(BagOfWordsImageCorpusBuilder.OTHER)
    else:
        subreddits = [subreddit]

    process_subreddits(subreddits, language_models, metrics, counters, output)


if __name__ == '__main__':
    cli()
