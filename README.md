# Introduction

This document contains two parts: first, an introduction (this section) which
includes a usage guide and comments about results, and, second, a development log,
which outlines, in chronological order, the steps taken in approaching this assignment.

## Usage

### Environment setup
To build the environment, ensure `virtualenv` is installed and run:

    source setup.sh

For subsequent activations of the environment, run

    source activate.sh


### Building language models
Given a list of posts called `safe_links_all` and a list of subreddits called `top100.txt`,
create `images.npz`, a file containing language models, by running the following:

    python image_extractor_lib.py build_bag_of_words_corpus safe_links_all top100.txt images.npz


### Ranking subreddits by similarity
Given a language model file `images.npz`, find subreddits similar to a given subreddit:

This commend find subreddits similar to `nba`, with the specified counter and metric, and
records the ranking in a directory within `output_dir`:

    python subreddit_similarity_lib.py rank images.npz --subreddit nba       \
                                                       --metric entropy      \
                                                       --counter l2_norm_bg  \
                                                       --output output_dir

The language models are large, so, for expediency, we allow multiple combinations
of subreddits, metrics, and counters can be run from a single instantiation.
* If `subreddit` is omitted, then all subreddits will be processed.
* If `metric` is omitted, then all _active_ metrics will be processed.
* If `counter` is omitted, then all _active_ counters will be processed.
* if `output` is omitted, then all results will be sent to `stdout`. Otherwise each
(subreddit, metric, counter) ranking is output to a single file.


### Best ranking parameters
The best results were found using entropy (i.e., KL-divergence) along with a distribution
builder that squares and normalizes counts to a probability distribution and penalizes
those counts against a secondary squared distribution which is compiled over the universe
of subreddits (images only).

    python subreddit_similarity_lib.py rank images.npz --metric entropy      \
                                                       --counter l2_norm_bg  \
                                                       --output /tmp


# Devlog

## Initial observations

### Dataset and processing constraints
Input is 42 GB, 207M records, roughly 25% of those records seem to be images

* Tom emphasized 8 hours of work for the assignment. With more time to work,
I would usually throw together several EDA (exploratory data analysis) pilot
experiments (e.g., entropy/perplexity measures, simple conditional/correlative
metrics, dimensionality reduction), but there may not be time and there may be
memory constraints at work. Additionally, in the typical case, I would leverage
parallel/distributed processing as memory constraints may come into play.


### Heterogeneity
Lots of subreddits; their organization is multimodal and over multiple dimensions

#### Problem
Dimensions that distinguish subreddits may absent or poorly represented in embedding
models (e.g., antonym relationships).
- If taking a CNN approach, have to figure out how to prevent over-fitting of low resource subreddits 
- If taking a language model approach, need to capture enough context to properly differentiate

##### Examples

###### content
* r/politics vs r/gaming
###### audience response
* r/funny vs r/aww
###### similar topic, differentiated by subreddit focus
* r/gaming vs r/cyberpunkgame
* r/parenting vs r/childfree
* r/parenting vs r/parentingfails
###### similar topic, differentiated by audience bias
* r/liberal vs r/conservative

#### Potential solutions
- Consider whether hierarchy may be extracted (e.g., through scraping) rather than learned.
- Train custom embedding model (e.g., using ELMo)
- Cast as graph-search: subreddits are nodes, edges represent the content of neighboring
  nodes (words, topics), something that allows us to walk to nodes whose content better
  explains the input (e.g., keywords, set of subreddits)


### Presence and importance of OOVs

#### Problem
Subreddits and their headlines are fundamentally concerned with entities
that are not meaningfully represented in the large-scale corpora on which
embedding models are typically based

##### Examples
* people, companies, products, games, songs, movies
* memes
* abbreviations/acronyms
* heterography

#### Potential Solutions
1. Using an embedding model in concert with an n-gram language model
   * Problem: Will unigram model capture sufficient context
1. Relying on n-gram model instead of embedding model
   * Problem: Lots of OOV words; OOV words may be essential in distinguishing subreddits
1. Constituent parsing
   * Problem: Can headlines be accurately parsed?
1. Entity-relation extractor
   * Problems: Time constraints, entity identification, entity resolution
1. Entity extractor
   * Problem: Time constraints, entity identification, and entities themselves may not be sufficient for task
1. Topic models
   * Problem: Time constraints

### N-Grams

#### Problem
##### bigrams, trigrams, etc.
* Do they dilute the training data too much?
* Could this be addressed by smoothing? Backing-off to a lower-n-gram model?
* Any benefit from representing subreddits by only encoding unambiguous cases? Extracting
 only `-ing` words, `-ed` words, and OOV words.
following pronouns, 



## Plan of Action

### Requirements
* Must be able to produce results within the required 8 hour time span
* Results must inform next steps, regardless of success

### Description
Compile a unigram language model for each subreddit of interest. Then, from one or more
"query subreddits", compile a language model; compare this language model to the language models
of all candidate subreddits, and rank them based on a similarity score. The score should be 
derived from a well-understood similarity metric used for comparing distributions,
such as KL-divergence, mutual information, conditional entropy, etc.

    score(query, subreddit) = similarity_metric(build_distribution(query.language_model),
                                                build_distribution(subreddit.language_model))

As there are many similarity measures, architecture should allow for maximal flexibility in
evaluating new metrics (for example, allowing a "sorter" to be associated with a metric,
to accommodate measures that return higher values for better matches).

Also allow for similar flexibility in how raw counts are processed into the distributions
used for comparison (these are the `DistributionBuilder` classes)

### Evaluation
This model trivially recognizes that a subreddit is maximally similar to itself,
so an "autoencoder" approach is not meaningful.

In the "top 100" dataset, there are hierarchies (e.g., `sports` being the parent of
`nfl`, `hockey`, `nba`, `soccer`), so a more meaningful approach to early evaluation
would be to observe whether these fundamental relationships are discovered by the model.

### Investigating parameters
#### Metrics
Using "entropy" (KL-divergence) as a similarity metric, promising results were observed with the
"sports" and "news" categories. Significantly noisier results were seen with "energy" metric,
so it was set aside for the sake of expediency. Mutual information seemed like a principled choice,
but results were not immediately good and performance was prohibitively poor using the
`sklearn` implementation, so it was also set aside in favor of entropy.
  
#### Distribution Builders
To avoid reprocessing the many gigabytes of raw input data, language models are compiled for
each subreddit in the "top subreddit" list provided, as well as a language model over all
subreddits. These language models are very simple: a single hash table for each subreddit,
which maps a token (i.e., a sequence of characters found in the training data) to a positive
integer indicating how many times that particular token was observed. These hash tables are
sparse, with the absence of an element indicating zero occurrences.

We know that comparing subreddits with raw counts will be problematic because subreddits have
drastically different representation in the training data. A popular "news" subreddit will include
the word "score" many more times than a niche subreddit focused on sports, however if we consider
the proportion of total words that the "score" accounts for:

     proportion("score") := count("score") / sum(count(w) for w in all_words)

We will find that proportion of "score" for the sports subreddit will be significantly higher than
proportion of "score" in the popular news subreddit. So instead of dealing with raw counts,
our distribution builder will normalize a subreddits counts so that the counts of all words
sum to 1.

TODO: Verify underflow is not occurring. If it does occur, use negative log values instead.

We see a significant improvement in using the normalized distribution builder. Next, we want
to consider approaches in getting rid of non-pertinent but high-presence tokens. Filtering
stop-words might be helpful here, but rather than taking a prescriptive approach, we'd like to
take an automatic, empirical strategy – something along the lines of reducing a token's count in
proportion to how popular that token is in the "universe" language model.

A simple (but problematic ) approach would be the `BackgroundReducingDistributionBuilder`,
which penalizes raw counts:

    count(w) = subreddit_count(w) / universe_count(w)

And testing shows this is not effective. A more promising approach will be to combine 
the normalization approach with the idea of a proportional penalty, which we attempt first with
the `NormalizedBackgroundReducingDistributionBuilder`. Here, the count is computed as follows:

    count(w) = max(min_score,
                   normalized_subreddit_count(w) - normalized_universe_count(w)

This approached offered substantially better results than any approach seen thus far,
with ideal ordering for "sports" and "news" categories. As an additional experiment, another
distribution builder was created, the `L2BackgroundReducingDistributionBuilder`, which
differs from the previous distribution builder in that it squares all counts before normalizing
(for both the individual subreddit's language model and the language model for the universe
of subreddits). While this left the top choices unchanged, it seemed to have a positive effect
on the secondary rankings, as the exponentiation serves to relocate probability mass to the
regions of highly-occurring words, away from lower occurrence. Further evaluation would be
required to determine whether this is beneficial – the underlying question being whether
the signal provided by low-occurrence tokens needs to be protected for optimal characterization.


#### Bonus tasks
##### Negative categories
In the setting of language models, functionality for "negative" or "counterexample" subreddits
could be achieved by zeroing counts in the derived language model. Language models can be created
through vector addition and subtraction, so just as a hybrid language model could be created by adding the
language models of `r/news` and `r/funny` (`vec_news` + `vec_funny`) so too could a hybrid language
model be created by subtraction, for example (`r/games` - `r/xbox`). An effect similar to regularization
can be achieved through exponentiation.

At the keyword level, counts can be scaled or zeroed. Synonyms can be obtained (e.g., via embedding models)
and have the same operations performed on them.

