# -*- coding: utf-8 -*-

from __future__ import division
from io import open
from collections import defaultdict, Counter
import re
import json
import click

import numpy as np
from numpy.core.umath import ceil

import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


class RowProcessor(object):
    def __init__(self, path):
        self.path = path

    def process_rows(self, process_row):
        with open(self.path, 'r') as f:
            for line in f:
                row = json.loads(line)
                process_row(row)


class BufferedRowProcessor(object):
    def __init__(self, path, limit=None, buffer_size=10000):
        self.path = path

        if limit:
            if limit < buffer_size:
                buffer_size = limit
                log.warn('buffer_size lowered to {} to meet limit'.format(limit))
            elif limit % buffer_size != 0:
                log.warn('limit is not a multiple of buffer size: processing will '
                         'end at row {:n}'.format(ceil(limit / buffer_size) * buffer_size))

        self.buffer_size = buffer_size
        self.limit = limit

    def process_rows(self, process_row):
        processed = 0
        queue = []
        remaining = self.buffer_size
        with open(self.path, 'r') as f:
            for line in f:
                queue.append(line)
                remaining -= 1

                # if queue is full, process the queued lines
                if remaining <= 0:
                    for queued_line in queue:
                        row = json.loads(queued_line)
                        process_row(row)
                    queue = []
                    remaining = self.buffer_size
                    if self.limit:
                        processed += self.buffer_size
                        if processed >= self.limit:
                            return


class TextProcessor(object):
    def __init__(self):
        self.text_patterns = []
        self._build_patterns()

    def _build_patterns(self):
        filter_expressions = ['[\t\n]',
                              r'(&amp;|&lt|&gt)',
                              r'[*#=_,?!.;:"\'/()\[\]–-]',
                              r'[ ]+']

        for expression in filter_expressions:
            self.text_patterns.append(re.compile(expression))

    def process_text(self, text):
        text = text.strip()
        for pattern in self.text_patterns:
            text = pattern.sub(' ', text)
        return [s for s in text.lower().split(' ') if s]


class BaseImageCorpusBuilder(object):
    def __init__(self, subreddits, row_processor, text_processor):
        self.subreddits = subreddits
        self.row_processor = row_processor
        self.text_processor = text_processor

        self.image_pattern = self._build_image_detector_pattern()

    @staticmethod
    def _build_image_detector_pattern():
        selectors = ['.jpg', '.jpeg', '.png', '.gif', '.bmp', '.tif', '.svg',
                     '1x.com', '500px', 'flickr', 'flic.kr', 'gfycat', 'giphy', 'imgur',
                     'instagram', 'instagr.am', 'picshd.com', 'picsimgessite.com',
                     'qkme.me', 'quickmeme', 'sharerpics.com', 'tinypic.com', 'twitpic',
                     '/s320', '/s400', '/s720', '/s1280', '/s1600',
                     'image', 'pics', 'photo']

        expression = '({})'.format('|'.join([s.replace('.', r'\.') for s in selectors]))
        return re.compile(expression)

    def process_row(self, row):
        raise NotImplementedError()


class BagOfWordsImageCorpusBuilder(BaseImageCorpusBuilder):
    ALL = '_ALL'
    OTHER = '_OTHER'

    def __init__(self, row_processor, text_processor, subreddits, output_file):
        super(self.__class__, self).__init__(subreddits=subreddits,
                                             row_processor=row_processor,
                                             text_processor=text_processor)
        self.word_counts_by_subreddit = defaultdict(Counter)
        self.output_file = output_file

        self.process_file()

    def process_row(self, row):
        # row = (subreddit, title, url, id)
        subreddit, text, url = row[0], row[1], row[2]

        # only process if this url has image indicators
        if not self.image_pattern.search(url):
            return

        words = self.text_processor.process_text(text)

        if subreddit in self.subreddits:
            self.word_counts_by_subreddit[subreddit].update(words)
        else:
            self.word_counts_by_subreddit[self.OTHER].update(words)

        self.word_counts_by_subreddit[self.ALL].update(words)

    def process_file(self):
        # process input
        log.info('processing links...')
        self.row_processor.process_rows(self.process_row)
        log.info('processing links done.')

        # save result
        log.info('saving result...')
        np.savez_compressed(self.output_file, **self.word_counts_by_subreddit)
        log.info('saving result done.')


def extract_subreddits(path):
    comment_indicator = '#'
    subreddits = set()
    with open(path, 'r') as f:
        lines = f.read().split('\n')

    for line in lines:
        line = line.strip()

        # ignore blank lines and comments
        if not line or line.startswith(comment_indicator):
            continue

        subreddits.add(line)
    return subreddits


@click.group()
def cli():
    pass


@cli.command('build_bag_of_words_corpus')
@click.argument('links', type=click.Path(exists=True))
@click.argument('top_subreddits', type=click.Path(exists=True))
@click.argument('output_file', type=click.STRING)
@click.option('--limit', type=click.INT, default=None)
def service_build_bag_of_words_corpus(links, top_subreddits, output_file, limit):
    subreddits = extract_subreddits(top_subreddits)
    BagOfWordsImageCorpusBuilder(row_processor=BufferedRowProcessor(links, limit),
                                 text_processor=TextProcessor(),
                                 subreddits=subreddits,
                                 output_file=output_file)


@cli.command('display_top_subreddits')
@click.argument('subreddits_file', type=click.STRING)
def service_display_top_subreddits(subreddits_file):
    subreddits = extract_subreddits(subreddits_file)
    print '\n'.join(sorted(subreddits))


if __name__ == '__main__':
    cli()
